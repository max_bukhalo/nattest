﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Exercise2 : MonoBehaviour
{
    [SerializeField] private Transform _camera;
    [SerializeField] private Transform _sphere;
    [SerializeField] private Transform _wayZone;
    [SerializeField] private List<ZoneTriger> _zones;
    [SerializeField] private GameObject _textPanel;
    [SerializeField] private TextMeshProUGUI _textMesh;
    [SerializeField] private string _nameExercise;
    [SerializeField] private string _description;


    private float _waitTimeForNextStep = 1f;
    private float _valueX;
    private float _valueY;
    private float _valueZ;
    private float _sphereRadius;
    // Start is called before the first frame update
    void Start()
    {
		_sphereRadius = _sphere.lossyScale.x;
        _wayZone.position = new Vector3(0f, _camera.position.y, _camera.position.z+3f);
		StartCoroutine(MoveBall());
    }

    private IEnumerator MoveBall()
    {
        _textPanel.SetActive(true);
        _textMesh.text = _nameExercise;
        yield return new WaitForSeconds(2f);
        _textMesh.text = _description;
        yield return new WaitForSeconds(4f);
        _textPanel.SetActive(false);

        while (true)
        {

            float x = Random.Range(-0.9f, 0.9f);
            float z = Random.Range(-0.4f, 0.4f);
            if (_zones[0].IsBallInZone == true)
            {
                float y = Random.Range(-0.2f, -0.45f);
                _sphere.localPosition = new Vector3(x, y, z);
            }
            else
            {
                float y = Random.Range(0.1f, 0.45f);
                _sphere.localPosition = new Vector3(x, y, z);
            }
            yield return new WaitForSeconds(_waitTimeForNextStep);
        }
	}
}
