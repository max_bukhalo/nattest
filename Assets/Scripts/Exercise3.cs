﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Exercise3 : MonoBehaviour
{
	[SerializeField] private Transform _spotLight;
	[SerializeField] private List<Transform> _positionLight;
	[SerializeField] private Color _activateColor;
	[SerializeField] private Color _deActivateColor;
	[SerializeField] private GameObject _textPanel;
	[SerializeField] private TextMeshProUGUI _textMesh;
	[SerializeField] private string _nameExercise;
	[SerializeField] private string _description;


	private ParticleSystem _particle;
	private int _positionSpotLight = 0;
	private float _waitTimeForNextStep = 3f;


	private void Start()
	{
		_particle = _spotLight.GetComponent<ParticleSystem>();
		_spotLight.position = _positionLight[_positionSpotLight].position;
		StartCoroutine(MoveSpotLight());
	}

	private IEnumerator MoveSpotLight()
	{
		_textPanel.SetActive(true);
		_textMesh.text = _nameExercise;
		yield return new WaitForSeconds(2f);
		_textMesh.text = _description;
		yield return new WaitForSeconds(4f);
		_textPanel.SetActive(false);
		while (true)
		{
			int valueSpot = Random.Range(0, 2);
			SetNewPosition(valueSpot);
			StartCoroutine(LightLevel());
			yield return new WaitForSeconds(_waitTimeForNextStep);
		}
	}

	private void SetNewPosition(int valueSpot)
	{
		if(valueSpot==0)
		{
			_positionSpotLight -= 1;
		}
		else
		{
			_positionSpotLight += 1;
		}
		ControlRange();
		_spotLight.position = _positionLight[_positionSpotLight].position;
	}

	private void ControlRange()
	{
		if(_positionSpotLight<0)
		{
			_positionSpotLight = _positionLight.Count - 1;
		}
		else if(_positionSpotLight> _positionLight.Count - 1)
		{
			_positionSpotLight = 0;
		}
	}

	private IEnumerator LightLevel()
	{
		_particle.startColor = _activateColor;
		yield return new WaitForSeconds(_waitTimeForNextStep / 2.65f);
		_particle.startColor = _deActivateColor;

	}
}
