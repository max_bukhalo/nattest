﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneTriger : MonoBehaviour
{
	public bool IsBallInZone;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == 11)
		{
			IsBallInZone = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer == 11)
		{
			IsBallInZone = false;
		}
	}
}
